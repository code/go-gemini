Go Gemini Library
=================

go-gemini is a library that provide an easy interface to create client and
servers that speak the Gemini protocol. The library is currently based on
version 0.9.2 of the spec but will be updated accordingly to new changes in the
protocol.


Example Server
--------------

The repository comes with an example server that respond with an hardcoded text
to the root page. To build the server run the following command:

    make build

Fork
----

This is a fork by Tom Ryder <tom@sanctum.geek.nz> that replaces the default
listening socket with systemd activation.
