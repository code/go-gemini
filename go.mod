module sanctum.geek.nz/code/go-gemini.git.git

go 1.12

require (
	github.com/coreos/go-systemd/v22 v22.1.0
	github.com/google/go-cmp v0.3.1
)
