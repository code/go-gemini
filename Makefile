all: build

build: gemini-example

gemini-example: cmd/example/*.go *.go
	go build -o gemini-example sanctum.geek.nz/code/go-gemini.git/cmd/example

clean:
	rm -rf gemini-example

.PHONY: all build clean
